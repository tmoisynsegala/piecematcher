import { config } from "./config";
import Grid from "./Grid";
import PlayButton from "./PlayButton";

//start only on page ready
document.addEventListener( "DOMContentLoaded", () =>
{
    new GameApplication();
} );

export class GameApplication
{

    /**
     *  Main Game Application
     */
    constructor()
    {
        //Initialize pixijs
        const app = new PIXI.Application(
            {
                backgroundColor: 0x1099bb
            } );
        document.body.appendChild( app.view );

        this.stage = app.stage;
        this.renderer = app.renderer;
        this.parentNode = app.view.parentNode;

        config.assets.atlas.forEach( path => app.loader.add( path ) );
        app.loader.load( () => this.setup() );

        window.addEventListener( "resize", () => this.onResize() );
    }

    /**
     * callback from asset loader, configure the scene
     * @param loader
     * @param resources
     */
    setup( loader, resources )
    {
        // Add grid
        this.grid = new Grid();
        this.grid.create();
        this.stage.addChild( this.grid );

        // Add play text
        this.playButton = new PlayButton( () => this.onReleasePlay(), () => this.onPressPlay() );
        this.playButton.x = this.renderer.width / 2;
        this.playButton.y = this.renderer.height - this.playButton.height / 2;
        this.stage.addChild( this.playButton );

        this.onResize();

        //matches the pieces
        this.grid.check();
    }

    //when the button is triggered, animate the button
    onPressPlay()
    {
        this.playButton.scale.set( 0.9 );
    }

    //restart the game
    async onReleasePlay()
    {
        this.playButton.scale.set( 1 );
        this.playButton.disable();
        await this.grid.switchAll();
        this.playButton.enable();
        this.grid.check();
    }

    onResize()
    {
        if ( this.grid )
        {
            this.renderer.resize( window.innerWidth, window.innerHeight );

            const gridSize = Math.min( this.renderer.width, this.renderer.height );
            this.grid.width = gridSize;
            this.grid.height  = gridSize;
            this.grid.x = ( this.renderer.width - this.grid.width ) / 2;
            this.grid.y = ( this.renderer.height - this.grid.height ) / 2;

            this.playButton.x = this.renderer.width / 2;
            this.playButton.y = this.renderer.height - this.playButton.height / 2;
        }
    }
}