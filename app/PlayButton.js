export default class PlayButton extends PIXI.Container
{
    constructor( onRelease, onPressed )
    {
        super();

        //Text Styling
        const style = new PIXI.TextStyle( {
            fontFamily: "Arial",
            fontSize: 36,
            fontStyle: "italic",
            fontWeight: "bold",
            fill: [ "#ffffff", "#00ff99" ], // gradient
            stroke: "#4a1850",
            strokeThickness: 5,
            dropShadow: true,
            dropShadowColor: "#000000",
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
            wordWrap: true,
            wordWrapWidth: 440
        } );

        //Text
        this.playText = new PIXI.Text( "Play", style );
        this.playText.anchor.set( 0.5 );
        this.playText.interactive = true;
        this.addChild( this.playText );

        if ( onRelease )
            this.playText.on( "pointerup", onRelease );
        if ( onPressed )
            this.playText.on( "pointerdown", onPressed );
    }

    disable() {
        this.playText.interactive = false;
    }

    enable() {
        this.playText.interactive = true;
    }
}