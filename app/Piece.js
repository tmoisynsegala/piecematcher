import { config } from "./config";
import { newSprite } from "./utils_idiomatic";

export default class Piece extends PIXI.Container
{
    constructor()
    {
        super();
        // positions on the matrix
        this.matrixX = 0;
        this.matrixY = 0;
        this.ARRAY_IMAGE_NAMES = Object.keys( config.assets.images );

        this.image = newSprite(this.getRandomImage());
        this.addChild(this.image);
    }

    getRandomImage(){

        //min ensure only configured number of images are created or number of images existent
        //floor ensure only whole numbers are used for positions
        let min = Math.min( config.setup.numberOfObjects, this.ARRAY_IMAGE_NAMES.length );
        let randomIndex = Math.floor( min * Math.random() );
        let imageName = this.ARRAY_IMAGE_NAMES[ randomIndex ];
        let nextId = config.assets.images[ imageName ];
        this.id = nextId;
        return nextId;
    }

    switchRandom(){
        let nextId = this.getRandomImage();
        this.id = nextId;
        this.image.texture = PIXI.Texture.from(nextId);
    }
}