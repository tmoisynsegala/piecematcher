export let config = {
    setup: {
        numberOfObjects: 5,
        numberOfMatches: 5,
        column: 10,
        line: 10
    },
    assets: {
        atlas: [
            "atlas.json"
        ],
        images: {
            panda: "rt_object_01",
            rainbow: "rt_object_02",
            unicorn: "rt_object_03",
            smile: "rt_object_04",
            guitar: "rt_object_05",
            candy: "rt_object_06",
            watermelon: "rt_object_07",
            radio: "rt_object_08"
        }
    }
};
