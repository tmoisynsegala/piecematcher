import { config } from "./config";
import Piece from "./Piece";

export default class Grid extends PIXI.Container
{
    /**
     * Grid controller, create pieces and matching
     */
    constructor()
    {
        super();
        this.matrix = [];
    }

    /**
     * Instantiate pieces
     */
    create()
    {
        for ( let y = 0; y < config.setup.column; y++ )
        {
            const line = [];
            this.matrix.push( line );
            for ( let x = 0; x < config.setup.line; x++ )
            {
                const piece = new Piece();
                piece.x = y * piece.width;
                piece.y = x * piece.height;
                piece.matrixX = x;
                piece.matrixY = y;
                this.addChild( piece );
                line.push( piece );
            }
        }
    }

    /**
     * Iterate over each element returning the name and position
     * @param callback( image, x, y )
     */
    each( callback )
    {
        this.matrix.forEach(
            ( line, x ) =>
                line.forEach( ( image, y ) => callback( image, x, y ) )
        );
    }

    /**
     * Switch all grid pieces
     */
    async switchAll()
    {
        return new Promise( async done =>
        {
            this.stopTweens();

            await this.fadePieces();

            this.each( piece => piece.switchRandom() );

            await this.showNewPieces();

            done();
        } );
    }

    stopTweens()
    {
        this.each( p =>
        {
            TweenMax.killTweensOf( p );
            p.alpha = 1;
        } );
    }

    check()
    {
        this.stopTweens();

        this.each(
            ( p, x, y ) =>
            {
                let result = [];
                this._match( p, x, y, result );
                if ( result.length >= config.setup.numberOfMatches )
                    result.forEach( piece => TweenMax.to( piece, 0.5, { alpha: 0.5, yoyo: true, repeat: -1 } ) );
            }
        );
    }

    _match( piece, x, y, result, origin )
    {
        result.push( piece );

        if ( x > 0 )//left
            if ( this.matrix[ x - 1 ][ y ].id === piece.id )
                if ( this.matrix[ x - 1 ][ y ] !== origin )
                    if ( result.indexOf( this.matrix[ x - 1 ][ y ] ) === -1 )
                        this._match( this.matrix[ x - 1 ][ y ], x - 1, y, result, piece );

        if ( y > 0 )//up
            if ( this.matrix[ x ][ y - 1 ].id === piece.id )
                if ( this.matrix[ x ][ y - 1 ] !== origin )
                    if ( result.indexOf( this.matrix[ x ][ y - 1 ] ) === -1 )
                        this._match( this.matrix[ x ][ y - 1 ], x, y - 1, result, piece );

        if ( x < this.matrix.length - 1 )//right
            if ( this.matrix[ x + 1 ][ y ].id === piece.id )
                if ( this.matrix[ x + 1 ][ y ] !== origin )
                    if ( result.indexOf( this.matrix[ x + 1 ][ y ] ) === -1 )
                        this._match( this.matrix[ x + 1 ][ y ], x + 1, y, result, piece );

        if ( y < this.matrix[ x ].length - 1 )//down
            if ( this.matrix[ x ][ y + 1 ].id === piece.id )
                if ( this.matrix[ x ][ y + 1 ] !== origin )
                    if ( result.indexOf( this.matrix[ x ][ y + 1 ] ) === -1 )
                        this._match( this.matrix[ x ][ y + 1 ], x, y + 1, result, piece );

        return result;
    }

    async fadePieces()
    {
        return new Promise( done =>
        {
            this.each( (p, x, y) =>
            {
                TweenMax.to( p.scale, 0.1, { delay: (x+y)/100, x: 0, y: 0, onComplete:() => done() } );
            } );
        } );
    }

    async showNewPieces()
    {
        return new Promise( done =>
        {
            this.each( (p, x, y) =>
            {
                TweenMax.to( p.scale, 0.1, { delay: (x+y)/100, x: 1, y: 1, onComplete:() => done() } );
            } );
        } );
    }
}
